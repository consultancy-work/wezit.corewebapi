﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckBookingServices
{
    public class TruckBookingService : ITruckBookingService
    {
        private readonly IGenericRepository<TruckBooking> _truckBookingRepository;
        private readonly wezitContext _context;
        public TruckBookingService(IGenericRepository<TruckBooking> truckBookingRepository,
            wezitContext context)
        {
            _truckBookingRepository = truckBookingRepository;
            _context = context;
        }

        public async Task<FeedbackMessage> GetByCustomer(int id)
        {
            var result = await _context.TruckBookings.Where(x => x.CustomerId.Equals(id))
                .Select(x => new TruckBookingDTO
                {
                    CurrentLocationId = x.CurrentLocationId,
                    CustomerId = x.CustomerId,
                    Customer = new CustomerDTO
                    {
                        Firstname = x.Customer.Firstname,
                        Lastname = x.Customer.Lastname
                    },
                    DestinationLocation = x.DestinationLocation,
                    LastUpdated = x.LastUpdated,
                    LoadCapacity = Convert.ToDouble(x.LoadCapacity),
                    LoadCategoryId = x.LoadCategoryId,
                    TruckBookingId = x.TruckBookingId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO
                    {
                        RegistrationNumber = x.Truck.RegistrationNumber,
                        TruckImeiNumber = x.Truck.TruckImeiNumber
                    },
                    TruckBookingEventLogs = x.TruckBookingEventLogs.Select(x => new TruckBookingEventLogDTO
                    {
                        BookingId = x.BookingId,
                        EventId = x.EventId,
                        EventStageId = x.EventStageId,
                        EventStage = new EventLogStageDTO
                        {
                            Description = x.EventStage.Description
                        }
                    }).ToList(),
                    SourceLocation = x.SourceLocation,
                    LoadDescription = x.LoadDescription,
                    ServiceProviders = _context.CustomerServiceProviders.Where(y => y.BookingId.Equals(x.TruckBookingId))
                    .Select(z => new ServiceProviderDTO
                    {
                        ServiceProviderId = z.ServiceProviderId,
                        ServiceProviderName = z.ServiceProvider.ServiceProviderName,
                        ServiceProviderTypeCode = z.ServiceProvider.ServiceProviderTypeCode
                    }).ToList()
                }).ToListAsync();


            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetActiveByCustomer(int id)
        {
            var result = await _context.TruckBookings.Where(x => x.CustomerId.Equals(id))
                .Select(x => new TruckBookingDTO
                {
                    CurrentLocationId = x.CurrentLocationId,
                    CustomerId = x.CustomerId,
                    Customer = new CustomerDTO
                    {
                        Firstname = x.Customer.Firstname,
                        Lastname = x.Customer.Lastname
                    },
                    DestinationLocation = x.DestinationLocation,
                    LastUpdated = x.LastUpdated,
                    LoadCapacity = Convert.ToDouble(x.LoadCapacity),
                    LoadCategoryId = x.LoadCategoryId,
                    TruckBookingId = x.TruckBookingId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO
                    {
                        RegistrationNumber = x.Truck.RegistrationNumber,
                        TruckImeiNumber = x.Truck.TruckImeiNumber
                    },
                    TruckBookingEventLogs = x.TruckBookingEventLogs.Select(x => new TruckBookingEventLogDTO
                    {
                        BookingId = x.BookingId,
                        EventId = x.EventId,
                        EventStageId = x.EventStageId,
                        EventStage = new EventLogStageDTO
                        {
                            Description = x.EventStage.Description
                        }
                    }).ToList(),
                    SourceLocation = x.SourceLocation,
                    LoadDescription = x.LoadDescription,
                    ServiceProviders = _context.CustomerServiceProviders.Where(y => y.BookingId.Equals(x.TruckBookingId))
                    .Select(z => new ServiceProviderDTO
                    {
                        ServiceProviderId = z.ServiceProviderId,
                        ServiceProviderName = z.ServiceProvider.ServiceProviderName,
                        ServiceProviderTypeCode = z.ServiceProvider.ServiceProviderTypeCode
                    }).ToList()
                }).ToListAsync();


            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetActiveByCompany(int id)
        {
            var result = await _context.TruckBookings.Where(x => x.Truck.TruckCompanyId.Equals(id)).
                Select(x => new TruckBookingDTO
                {
                    CurrentLocationId = x.CurrentLocationId,
                    CustomerId = x.CustomerId,
                    Customer = new CustomerDTO
                    {
                        Firstname = x.Customer.Firstname,
                        Lastname = x.Customer.Lastname
                    },
                    DestinationLocation = x.DestinationLocation,
                    LastUpdated = x.LastUpdated,
                    LoadCapacity = Convert.ToDouble(x.LoadCapacity),
                    LoadCategoryId = x.LoadCategoryId,
                    TruckBookingId = x.TruckBookingId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO
                    {
                        RegistrationNumber = x.Truck.RegistrationNumber,
                        TruckImeiNumber = x.Truck.TruckImeiNumber
                    },
                    TruckBookingEventLogs = x.TruckBookingEventLogs.Select(x => new TruckBookingEventLogDTO
                    {
                        BookingId = x.BookingId,
                        EventId = x.EventId,
                        EventStageId = x.EventStageId,
                        EventStage = new EventLogStageDTO
                        {
                            Description = x.EventStage.Description
                        }
                    }).ToList(),
                    SourceLocation = x.SourceLocation,
                    LoadDescription = x.LoadDescription,
                    ServiceProviders = _context.CustomerServiceProviders.Where(y => y.BookingId.Equals(x.TruckBookingId))
                    .Select(z => new ServiceProviderDTO
                    {
                        ServiceProviderId = z.ServiceProviderId,
                        ServiceProviderName = z.ServiceProvider.ServiceProviderName,
                        ServiceProviderTypeCode = z.ServiceProvider.ServiceProviderTypeCode
                    }).ToList()
                }).ToListAsync();
            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetByCompany(int id)
        {
            var result = await _context.TruckBookings.Where(x => x.Truck.TruckCompanyId.Equals(id)).
                Select(x => new TruckBookingDTO
                {
                    CurrentLocationId = x.CurrentLocationId,
                    CustomerId = x.CustomerId,
                    Customer = new CustomerDTO
                    {
                        Firstname = x.Customer.Firstname,
                        Lastname = x.Customer.Lastname
                    },
                    DestinationLocation = x.DestinationLocation,
                    LastUpdated = x.LastUpdated,
                    LoadCapacity = Convert.ToDouble(x.LoadCapacity),
                    LoadCategoryId = x.LoadCategoryId,
                    TruckBookingId = x.TruckBookingId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO
                    {
                        RegistrationNumber = x.Truck.RegistrationNumber,
                        TruckImeiNumber = x.Truck.TruckImeiNumber
                    },
                    TruckBookingEventLogs = x.TruckBookingEventLogs.Select(x => new TruckBookingEventLogDTO
                    {
                        BookingId = x.BookingId,
                        EventId = x.EventId,
                        EventStageId = x.EventStageId,
                        EventStage = new EventLogStageDTO
                        {
                            Description = x.EventStage.Description
                        }
                    }).ToList(),
                    SourceLocation = x.SourceLocation,
                    LoadDescription = x.LoadDescription,
                    ServiceProviders = _context.CustomerServiceProviders.Where(y => y.BookingId.Equals(x.TruckBookingId))
                    .Select(z => new ServiceProviderDTO
                    {
                        ServiceProviderId = z.ServiceProviderId,
                        ServiceProviderName = z.ServiceProvider.ServiceProviderName,
                        ServiceProviderTypeCode = z.ServiceProvider.ServiceProviderTypeCode
                    }).ToList()
                }).ToListAsync();
            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetTruckBooking(int id)
        {
            var result = await _context.TruckBookings.Where(x => x.TruckBookingId.Equals(id))
               .Select(x => new TruckBookingDTO
               {
                   CurrentLocationId = x.CurrentLocationId,
                   CustomerId = x.CustomerId,
                   Customer = new CustomerDTO
                   {
                       Firstname = x.Customer.Firstname,
                       Lastname = x.Customer.Lastname
                   },
                   DestinationLocation = x.DestinationLocation,
                   LastUpdated = x.LastUpdated,
                   LoadCapacity = Convert.ToDouble(x.LoadCapacity),
                   LoadCategoryId = x.LoadCategoryId,
                   TruckBookingId = x.TruckBookingId,
                   TruckId = x.TruckId,
                   MotorClinicProviderServiceId = x.MotorClinicProviderServiceId,
                   MotorClinicProviderServiceName = x.MotorClinicProviderService.ServiceProviderName,
                   Truck = new TruckDTO
                   {
                       RegistrationNumber = x.Truck.RegistrationNumber,
                       TruckImeiNumber = x.Truck.TruckImeiNumber
                       
                   },
                   TruckBookingEventLogs = x.TruckBookingEventLogs.Select(x => new TruckBookingEventLogDTO
                   {
                       BookingId = x.BookingId,
                       EventId = x.EventId,
                       EventStageId = x.EventStageId,
                       EventStage = new EventLogStageDTO
                       {
                           Description = x.EventStage.Description
                       }
                   }).ToList(),
                   SourceLocation = x.SourceLocation,
                   LoadDescription = x.LoadDescription,
                   ServiceProviders = _context.CustomerServiceProviders.Where(y => y.BookingId.Equals(x.TruckBookingId))
                   .Select(z => new ServiceProviderDTO
                   {
                       ServiceProviderId = z.ServiceProviderId,
                       ServiceProviderName = z.ServiceProvider.ServiceProviderName,
                       ServiceProviderTypeCode = z.ServiceProvider.ServiceProviderTypeCode
                   }).ToList()
               }).FirstOrDefaultAsync();

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetActiveList()
        {
            var relationsToQuery = new string[] { "Customer", "Truck" };
            var data = await _context.TruckBookings.
                Select(x => new TruckBookingDTO
                {
                    CurrentLocationId = x.CurrentLocationId,
                    CustomerId = x.CustomerId,
                    Customer = new CustomerDTO
                    {
                        Firstname = x.Customer.Firstname,
                        Lastname = x.Customer.Lastname
                    },
                    DestinationLocation = x.DestinationLocation,
                    LastUpdated = x.LastUpdated,
                    LoadCapacity = Convert.ToDouble(x.LoadCapacity),
                    LoadCategoryId = x.LoadCategoryId,
                    TruckBookingId = x.TruckBookingId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO
                    {
                        RegistrationNumber = x.Truck.RegistrationNumber,
                        TruckImeiNumber = x.Truck.TruckImeiNumber
                    },
                    TruckBookingEventLogs = x.TruckBookingEventLogs.Select(x => new TruckBookingEventLogDTO
                    {
                        BookingId = x.BookingId,
                        EventId = x.EventId,
                        EventStageId = x.EventStageId,
                        EventStage = new EventLogStageDTO
                        {
                            Description = x.EventStage.Description
                        }
                    }).ToList(),
                    SourceLocation = x.SourceLocation,
                    LoadDescription = x.LoadDescription
                }).ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
        public async Task<FeedbackMessage> GetTruckBookingsList()
        {
            var relationsToQuery = new string[] { "Customer", "Truck" };
            var data = await _context.TruckBookings.
                Select(x => new TruckBookingDTO
                {
                    CurrentLocationId = x.CurrentLocationId,
                    CustomerId = x.CustomerId,
                    Customer = new CustomerDTO
                    {
                        Firstname = x.Customer.Firstname,
                        Lastname = x.Customer.Lastname
                    },
                    DestinationLocation = x.DestinationLocation,
                    LastUpdated = x.LastUpdated,
                    LoadCapacity = Convert.ToDouble(x.LoadCapacity),
                    LoadCategoryId = x.LoadCategoryId,
                    TruckBookingId = x.TruckBookingId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO
                    {
                        RegistrationNumber = x.Truck.RegistrationNumber,
                        TruckImeiNumber = x.Truck.TruckImeiNumber
                    },
                    TruckBookingEventLogs = x.TruckBookingEventLogs.Select(x => new TruckBookingEventLogDTO
                    {
                        BookingId = x.BookingId,
                        EventId = x.EventId,
                        EventStageId = x.EventStageId,
                        EventStage = new EventLogStageDTO
                        {
                            Description = x.EventStage.Description
                        }
                    }).ToList(),
                    SourceLocation = x.SourceLocation,
                    LoadDescription = x.LoadDescription
                }).ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateTruckBooking(TruckBookingRequest truckBookingRequest)
        {
            //TODO algorithm to get the closest truck of type sent
            var truckId = await _context.TrucksSubscribeds.Where(x => x.Approved == true && x.Truck.TruckTypeId.Equals(truckBookingRequest.TruckTypeId))
                .Select(x => x.TruckId).FirstOrDefaultAsync();

            if (truckId == 0)
            {
                return (new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "No truck found with your specifications"
                });
            }
            var truckBooking = new TruckBooking
            {
                TruckId = truckId,
                CustomerId = Convert.ToInt32(truckBookingRequest.CustomerId),
                SourceLocation = truckBookingRequest.Source,
                DestinationLocation = truckBookingRequest.Destination,
                LastUpdated = DateTime.UtcNow,
                LoadCapacity = truckBookingRequest.LoadCapacity,
                LoadCategoryId = truckBookingRequest.LoadTypeId,
                MotorClinicProviderServiceId = truckBookingRequest.MotorClinicProviderServiceId
            };

            await _context.AddAsync(truckBooking);
            await _context.SaveChangesAsync();
            await _context.TruckBookingEventLogs.AddAsync(new TruckBookingEventLog
            {
                BookingId = truckBooking.TruckBookingId,
                EventStageId = 1,
            });

            await _context.SaveChangesAsync();
            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Message = "Successfully created a booking",
                Data = truckBooking
            };
        }

        public async Task<FeedbackMessage> UpdateTruckBooking(int id, int eventStageId, TruckBookingDTO truckBookingDTO)
        {
            var feedbackMessage = await GetTruckBooking(id);
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }
            var truckBooking = feedbackMessage.Data as TruckBooking;

            truckBooking.TruckId = truckBookingDTO.TruckId;
            truckBooking.MotorClinicProviderServiceId = truckBookingDTO.MotorClinicProviderServiceId;
            truckBooking.LastUpdated = DateTime.UtcNow;

            var result = await _truckBookingRepository.Update(truckBooking);

            await _context.TruckBookingEventLogs.AddAsync(new TruckBookingEventLog
            {
                BookingId = Convert.ToInt32(id),
                EventStageId = eventStageId,
            });

            await _context.SaveChangesAsync();

            return result;
        }

        public async Task<FeedbackMessage> DeleteTruckBooking(int id)
        {
            var result = await _truckBookingRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }
    }
}
