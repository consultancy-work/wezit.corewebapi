﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckBookingServices
{
    public interface ITruckBookingService
    {
        Task<FeedbackMessage> GetTruckBooking(int id);
        Task<FeedbackMessage> GetByCustomer(int id);
        Task<FeedbackMessage> GetActiveByCustomer(int id);
        Task<FeedbackMessage> GetByCompany(int id);
        Task<FeedbackMessage> GetActiveByCompany(int id);
        Task<FeedbackMessage> GetTruckBookingsList();
        Task<FeedbackMessage> GetActiveList();

        Task<FeedbackMessage> CreateTruckBooking(TruckBookingRequest truckBookingRequest);

        Task<FeedbackMessage> UpdateTruckBooking(int id,int eventStageId, TruckBookingDTO truckBookingDTO);

        Task<FeedbackMessage> DeleteTruckBooking(int id);
    }
}
