﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.CustomerServices
{
    public interface ICustomerService
    {
        Task<FeedbackMessage> GetCustomer(long id);

        Task<FeedbackMessage> GetCustomersList();

        Task<FeedbackMessage> CreateCustomer(CustomerDTO customerDTO);

        Task<FeedbackMessage> UpdateCustomer(long id, CustomerDTO customerDTO);

        Task<FeedbackMessage> DeleteCustomer(long id);
    }
}
