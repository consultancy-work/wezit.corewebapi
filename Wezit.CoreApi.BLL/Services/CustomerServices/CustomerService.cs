﻿using System;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.CustomerServices
{
    public class CustomerService : ICustomerService
    {

        private readonly IGenericRepository<Customer> _customerRepository;

        public CustomerService(IGenericRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;

        }

        public async Task<FeedbackMessage> GetCustomer(long id)
        {
            var result = await _customerRepository.Get(Convert.ToInt32(id));

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetCustomersList()
        {
            var data = await _customerRepository.GetAll();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateCustomer(CustomerDTO customerDTO)
        {
            var customer = new Customer
            {
                Firstname = customerDTO.Firstname,
                Lastname = customerDTO.Lastname,
                EmailAddress = customerDTO.EmailAddress,
                PhoneNumber = customerDTO.PhoneNumber,
                PhysicalAddress = customerDTO.PhysicalAddress,
                PostalAddress = customerDTO.PostalAddress,
                CompanyName = customerDTO.CompanyName,
                City = customerDTO.City,
                CountryId = customerDTO.CountryId,
                CustomerIdPath = customerDTO.CustomerIdPath,
                CompanyRegDocPath = customerDTO.CompanyRegDocPath,
                IsCompany = customerDTO.IsCompany,
                IsVerified = customerDTO.IsVerified
            };

            var result = await _customerRepository.Insert(customer);
            return result;
        }

        public async Task<FeedbackMessage> UpdateCustomer(long id, CustomerDTO customerDTO)
        {
            var feedbackMessage = await GetCustomer(id);
            var customer = feedbackMessage.Data as Customer;
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }

            customer.Firstname = customerDTO.Firstname;
            customer.Lastname = customerDTO.Lastname;
            customer.EmailAddress = customerDTO.EmailAddress;
            customer.PhoneNumber = customerDTO.PhoneNumber;
            customer.PhysicalAddress = customerDTO.PhysicalAddress;
            customer.PostalAddress = customerDTO.PostalAddress;
            customer.CompanyName = customerDTO.CompanyName;
            customer.City = customerDTO.City;
            customer.CountryId = customerDTO.CountryId;
            customer.CustomerIdPath = customerDTO.CustomerIdPath;
            customer.CompanyRegDocPath = customerDTO.CompanyRegDocPath;
            customer.IsCompany = customerDTO.IsCompany;
            customer.IsVerified = customerDTO.IsVerified;

            var result = await _customerRepository.Update(customer);

            return result;
        }

        public async Task<FeedbackMessage> DeleteCustomer(long id)
        {
            var result = await _customerRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }
    }
}
