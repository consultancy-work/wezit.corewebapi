﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TrucksServices
{
    public interface ITruckService
    {
        Task<FeedbackMessage> GetTruck(long id);

        Task<FeedbackMessage> GetTrucksList();
        Task<FeedbackMessage> GetCategoryList();
        Task<FeedbackMessage> GetTruckTypeList();
        Task<FeedbackMessage> GetTrucksByCompany(int companyId);

        Task<FeedbackMessage> CreateTruck(TruckDTO truckDTO);

        Task<FeedbackMessage> UpdateTruck(long id, TruckDTO truckDTO);

        Task<FeedbackMessage> DeleteTruck(long id);
    }
}
