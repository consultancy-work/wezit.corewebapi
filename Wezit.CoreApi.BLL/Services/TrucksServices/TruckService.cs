﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TrucksServices
{
    public class TruckService : ITruckService
    {
        private readonly IGenericRepository<Truck> _truckRepository;
        private readonly wezitContext _context;

        public TruckService(IGenericRepository<Truck> truckRepository, wezitContext context)
        {
            _truckRepository = truckRepository;
            _context = context;
        }
        public async Task<FeedbackMessage> GetTruck(long id)
        {
            var relationsToQuery = new string[] { "TruckCompany" };
            var result = await _truckRepository.Get("TruckId", Convert.ToString(id), 1, SearchType.Numeric, relationsToQuery);

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetTruckTypeList()
        {
            var data = await _context.TruckTypes.ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
        public async Task<FeedbackMessage> GetCategoryList()
        {
            var data = await _context.DeliveryCategories.ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
        public async Task<FeedbackMessage> GetTrucksList()
        {
            var relationsToQuery = new string[] { "TruckCompany", "TrucksSubscribed", "TruckBooking" };
            var data = await _truckRepository.GetList("", "", 100, SearchType.Numeric, relationsToQuery);

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
        public async Task<FeedbackMessage> GetTrucksByCompany(int companyId)
        {
            var relationsToQuery = new string[] { };
            var data = await _context.Trucks.Where(x => x.TruckCompanyId.Equals(companyId)).ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
        public async Task<FeedbackMessage> CreateTruck(TruckDTO truckDTO)
        {
            var truck = new Truck
            {
                TruckCompanyId = truckDTO.TruckCompanyId,
                RegistrationNumber = truckDTO.RegistrationNumber,
                Capacity = truckDTO.Capacity,
                CurrentLocationId = truckDTO.CurrentLocationId,
                TruckImeiNumber = truckDTO.TruckImeiNumber
            };

            var result = await _truckRepository.Insert(truck);
            return result;
        }
        public async Task<FeedbackMessage> UpdateTruck(long id, TruckDTO truckDTO)
        {
            var feedbackMessage = await GetTruck(id);
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }
            var truck = feedbackMessage.Data as Truck;

            truck.TruckCompanyId = truckDTO.TruckCompanyId;
            truck.RegistrationNumber = truckDTO.RegistrationNumber;
            truck.Capacity = truckDTO.Capacity;
            truck.CurrentLocationId = truckDTO.CurrentLocationId;
            truck.TruckImeiNumber = truckDTO.TruckImeiNumber;

            var result = await _truckRepository.Update(truck);

            return result;
        }
        public async Task<FeedbackMessage> DeleteTruck(long id)
        {
            var result = await _truckRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }

    }
}
