﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.ServiceProvidersServices
{
    public interface ICustomerServiceProviderService
    {
        Task<FeedbackMessage> CreateCustomerServiceProvider(IEnumerable<CustomerServiceProviderDTO> customerServiceProviders);
        Task<FeedbackMessage> GetCustomerServiceProvider(int companyId);
        Task<FeedbackMessage> GetCustomerServiceProviderByBooking(int bookingId);
        Task<FeedbackMessage> UpdateCustomerServiceProvider(IEnumerable<CustomerServiceProviderDTO> customerServiceProviders);
    }
}