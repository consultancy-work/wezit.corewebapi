﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.DataModels.ViewModels;

namespace Wezit.CoreApi.BLL.Services.ServiceProvidersServices
{
    public class CustomerServiceProviderService : ICustomerServiceProviderService
    {
        private readonly wezitContext _context;

        public CustomerServiceProviderService(wezitContext context)
        {
            _context = context;
        }

        public async Task<FeedbackMessage> GetCustomerServiceProvider(int companyId)
        {
            var result = await _context.CustomerServiceProviders.Where(x => x.CustomerId.Equals(companyId))
                .Select(x => new CustomerServiceProviderDTO
                {
                    CustomerProviderId = x.CustomerProviderId,
                    ServiceProviderId = x.ServiceProviderId,
                    ServiceProviderName = x.ServiceProvider.ServiceProviderName,
                    CustomerId = x.CustomerId,
                    BookingId = x.BookingId,
                    ServiceProviders = _context.ServiceProviders.Select(y => new ServiceProviderDTO
                    {
                        ServiceProviderId = y.ServiceProviderId,
                        ServiceProviderName = y.ServiceProviderName,
                        ServiceProviderTypeCode = y.ServiceProviderTypeCode
                    }).ToList()
                }).ToListAsync();
            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetCustomerServiceProviderByBooking(int bookingId)
        {
            var result = await _context.CustomerServiceProviders.Where(x => x.BookingId.Equals(bookingId))
                .Select(x => new CustomerServiceProviderDTO
                {
                    CustomerProviderId = x.CustomerProviderId,
                    ServiceProviderId = x.ServiceProviderId,
                    ServiceProviderName = x.ServiceProvider.ServiceProviderName,
                    CustomerId = x.CustomerId,
                    BookingId = x.BookingId,
                    ServiceProviders = _context.ServiceProviders.Select(y => new ServiceProviderDTO
                    {
                        ServiceProviderId = y.ServiceProviderId,
                        ServiceProviderName = y.ServiceProviderName,
                        ServiceProviderTypeCode = y.ServiceProviderTypeCode
                    }).ToList()
                }).ToListAsync();
            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> CreateCustomerServiceProvider(IEnumerable<CustomerServiceProviderDTO> customerServiceProviders)
        {
            var customerServiceProviderList = new List<CustomerServiceProvider>();
            foreach (var provider in customerServiceProviders)
            {
                customerServiceProviderList.Add(new CustomerServiceProvider
                {
                    ServiceProviderId = provider.ServiceProviderId,
                    CustomerId = provider.CustomerId
                });
            }


            await _context.AddRangeAsync(customerServiceProviderList);
            await _context.SaveChangesAsync();
            return new FeedbackMessage
            {
                HasErrorOccured = false
            };
        }

        public async Task<FeedbackMessage> UpdateCustomerServiceProvider(IEnumerable<CustomerServiceProviderDTO> customerServiceProviders)
        {
            var customerServiceProviderList = new List<CustomerServiceProvider>();
            foreach (var provider in customerServiceProviders)
            {
                customerServiceProviderList.Add(new CustomerServiceProvider
                {
                    ServiceProviderId = provider.ServiceProviderId,
                    CustomerId = provider.CustomerId
                });
            }

            _context.UpdateRange(customerServiceProviderList);
            await _context.SaveChangesAsync();
            return new FeedbackMessage
            {
                HasErrorOccured = false
            };
        }

    }
}
