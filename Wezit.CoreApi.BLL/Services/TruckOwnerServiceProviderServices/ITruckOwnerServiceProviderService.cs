﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckOwnerServiceProvidersServices
{
    public interface ITruckOwnerServiceProviderService
    {
        Task<FeedbackMessage> CreateTruckOwnerServiceProvider(IEnumerable<TruckOwnerServiceProviderDTO> truckOwnerServiceProviders);
        Task<FeedbackMessage> GetTruckOwnerServiceProvider(int truckCompanyId);
        Task<FeedbackMessage> UpdateTruckOwnerServiceProvider(IEnumerable<TruckOwnerServiceProviderDTO> truckOwnerServiceProviders);
    }
}