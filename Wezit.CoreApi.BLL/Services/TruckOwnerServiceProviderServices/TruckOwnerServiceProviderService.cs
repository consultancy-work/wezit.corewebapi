﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.DataModels.ViewModels;

namespace Wezit.CoreApi.BLL.Services.TruckOwnerServiceProvidersServices
{
    public class TruckOwnerServiceProviderService : ITruckOwnerServiceProviderService
    {
        private readonly wezitContext _context;

        public TruckOwnerServiceProviderService(wezitContext context)
        {
            _context = context;
        }

        public async Task<FeedbackMessage> GetTruckOwnerServiceProvider(int truckCompanyId)
        {
            var result = await _context.TruckOwnerServiceProviders.Where(x => x.TruckOwnerId.Equals(truckCompanyId))
                .Select(x => new TruckOwnerServiceProviderDTO
                {
                    TruckOwnerProviderId = x.TruckOwnerProviderId,
                    ServiceProviderId = x.ServiceProviderId,
                    ServiceProviderName = x.ServiceProvider.ServiceProviderName,
                    ServiceProviders = _context.ServiceProviders.Select(y => new ServiceProviderDTO
                    {
                        ServiceProviderId = y.ServiceProviderId,
                        ServiceProviderName = y.ServiceProviderName,
                        ServiceProviderTypeCode = y.ServiceProviderTypeCode
                    }).ToList()
                }).ToListAsync();
            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> CreateTruckOwnerServiceProvider(IEnumerable<TruckOwnerServiceProviderDTO> truckOwnerServiceProviders)
        {
            var truckOwnerServiceProviderList = new List<TruckOwnerServiceProvider>();
            foreach (var provider in truckOwnerServiceProviders)
            {
                truckOwnerServiceProviderList.Add(new TruckOwnerServiceProvider
                {
                    ServiceProviderId = provider.ServiceProviderId,
                    TruckOwnerId = provider.TruckOwnerId
                });
            }


            await _context.AddRangeAsync(truckOwnerServiceProviderList);
            await _context.SaveChangesAsync();
            return new FeedbackMessage
            {
                HasErrorOccured = false
            };
        }

        public async Task<FeedbackMessage> UpdateTruckOwnerServiceProvider(IEnumerable<TruckOwnerServiceProviderDTO> truckOwnerServiceProviders)
        {
            var truckOwnerServiceProviderList = new List<TruckOwnerServiceProvider>();
            foreach (var provider in truckOwnerServiceProviders)
            {
                truckOwnerServiceProviderList.Add(new TruckOwnerServiceProvider
                {
                    ServiceProviderId = provider.ServiceProviderId,
                    TruckOwnerId = provider.TruckOwnerId
                });
            }

            _context.UpdateRange(truckOwnerServiceProviderList);
            await _context.SaveChangesAsync();
            return new FeedbackMessage
            {
                HasErrorOccured = false
            };
        }

    }
}
