﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckBookingEventLogServices
{
    public interface ITruckBookingEventLogService
    {
        Task<FeedbackMessage> GetTruckBookingEventLog(long id);

        Task<FeedbackMessage> GetTruckBookingEventLogsList();

        Task<FeedbackMessage> CreateTruckBookingEventLog(TruckBookingEventLogDTO truckBookingEventLogDTO);

        Task<FeedbackMessage> UpdateTruckBookingEventLog(long id, TruckBookingEventLogDTO truckBookingEventLogDTO);

        Task<FeedbackMessage> DeleteTruckBookingEventLog(long id);
    }
}
