﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckBookingEventLogServices
{
    public class TruckBookingEventLogService : ITruckBookingEventLogService
    {
        private readonly IGenericRepository<TruckBookingEventLog> _truckBookingEventLogRepository;

        public TruckBookingEventLogService(IGenericRepository<TruckBookingEventLog> truckBookingEventLogRepository)
        {
            _truckBookingEventLogRepository = truckBookingEventLogRepository;

        }

        public async Task<FeedbackMessage> GetTruckBookingEventLog(long id)
        {
            var result = await _truckBookingEventLogRepository.Get(Convert.ToInt32(id));

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetTruckBookingEventLogsList()
        {
            var data = await _truckBookingEventLogRepository.GetAll();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateTruckBookingEventLog(TruckBookingEventLogDTO truckBookingEventLogDTO)
        {
            var truckBookingEventLog = new TruckBookingEventLog
            {
                BookingId = truckBookingEventLogDTO.BookingId,
                EventStageId = truckBookingEventLogDTO.EventStageId
            };

            var result = await _truckBookingEventLogRepository.Insert(truckBookingEventLog);
            return result;
        }

        public async Task<FeedbackMessage> UpdateTruckBookingEventLog(long id, TruckBookingEventLogDTO truckBookingEventLogDTO)
        {
            var feedbackMessage = await GetTruckBookingEventLog(id);
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }
            var truckBookingEventLog = feedbackMessage.Data as TruckBookingEventLog;

            truckBookingEventLog.BookingId = truckBookingEventLogDTO.BookingId;
            truckBookingEventLog.EventStageId = truckBookingEventLogDTO.EventStageId;

            var result = await _truckBookingEventLogRepository.Update(truckBookingEventLog);

            return result;
        }

        public async Task<FeedbackMessage> DeleteTruckBookingEventLog(long id)
        {
            var result = await _truckBookingEventLogRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }
    }
}
