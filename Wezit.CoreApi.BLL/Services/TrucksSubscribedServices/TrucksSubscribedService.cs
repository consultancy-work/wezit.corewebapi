﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TrucksSubscribedServices
{
    public class TrucksSubscribedService: ITrucksSubscribedService 
    { 

        private readonly IGenericRepository<TrucksSubscribed> _trucksSubscribedRepository;
        private readonly wezitContext _context;

        public TrucksSubscribedService(IGenericRepository<TrucksSubscribed> trucksSubscribedRepository,
            wezitContext context)
        {
            _trucksSubscribedRepository = trucksSubscribedRepository;
            _context = context;
        }

        public async Task<FeedbackMessage> GetTrucksSubscribed(int id)
        {
            var result = await _context.TrucksSubscribeds.Where(x => x.SubscriptionId.Equals(id))
                .Select(x => new TrucksSubscribedDTO
                {
                    Approved = x.Approved,
                    SubscriptionId = x.SubscriptionId,
                    TruckId = x.TruckId,
                    TruckSubscriptionId = x.TruckSubscriptionId,
                    Truck = new TruckDTO
                    {
                        TruckId = x.Truck.TruckId,
                        RegistrationNumber = x.Truck.RegistrationNumber
                    }
                    
                }).FirstOrDefaultAsync();

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetTrucksSubscribedList()
        {
            var data = await _context.TrucksSubscribeds
                .Select(x => new TrucksSubscribedDTO
                {
                    Approved = x.Approved,
                    SubscriptionId = x.SubscriptionId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO
                    {
                        TruckId = x.Truck.TruckId,
                        RegistrationNumber = x.Truck.RegistrationNumber
                    }
                }).ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> GetTrucksSubscribedByCompany(int companyId)
        {
            var relationsToQuery = new string[] { };
            //var data = await _trucksSubscribedRepository.GetList("TruckCompanyId", Convert.ToString(companyId), 1, SearchType.Numeric, relationsToQuery);
            var data = await _context.TrucksSubscribeds.Where(x => x.TruckSubscription.TruckCompanyId.Equals(companyId))
                .Select(x => new TrucksSubscribedDTO
                {
                    Approved = x.Approved,
                    SubscriptionId = x.SubscriptionId,
                    TruckId = x.TruckId,
                    Truck = new TruckDTO 
                    {
                        TruckId = x.Truck.TruckId,
                        RegistrationNumber = x.Truck.RegistrationNumber
                    }
                }).ToListAsync();


            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
       

        public async Task<FeedbackMessage> CreateTrucksSubscribed(TrucksSubscribedDTO trucksSubscribedDTO)
        {
             var isFound = await _context.TrucksSubscribeds.AnyAsync(x => x.TruckId.Equals(trucksSubscribedDTO.TruckId) && x.TruckSubscriptionId.Equals(trucksSubscribedDTO.TruckSubscriptionId));
            if (isFound)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The truck is already added to the subscription"
                };
            }

            var subscriptionCount = await _context.TrucksSubscribeds.Where(x => x.SubscriptionId.Equals(trucksSubscribedDTO.SubscriptionId)).CountAsync();
            var bandLimit = await _context.TrucksSubscribeds.Where(x => x.SubscriptionId.Equals(trucksSubscribedDTO.SubscriptionId))
                .Select(x => x.TruckSubscription.Band.BandCap).FirstOrDefaultAsync();
            
            /*if (bandLimit == subscriptionCount)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = $"The trucks for this suscription have reached their limit, only {bandLimit} can be registered. Please upgrade your package to register more trucks."
                };
            }*/

            var trucksSubscribed = new TrucksSubscribed
            {
                TruckId = trucksSubscribedDTO.TruckId,
                TruckSubscriptionId = trucksSubscribedDTO.TruckSubscriptionId
            };

            var result = await _trucksSubscribedRepository.Insert(trucksSubscribed);
            return result;
        }

        public async Task<FeedbackMessage> UpdateTrucksSubscribed(int id, TrucksSubscribedDTO trucksSubscribedDTO)
        {
            var trucksSubscribed = await _context.TrucksSubscribeds.Where(x => x.SubscriptionId.Equals(id)).FirstOrDefaultAsync();
            
            trucksSubscribed.TruckId = trucksSubscribedDTO.TruckId;
            trucksSubscribed.Approved = trucksSubscribedDTO.Approved;

            var result = await _trucksSubscribedRepository.Update(trucksSubscribed);

            return result;
        }

        public async Task<FeedbackMessage> DeleteTrucksSubscribed(int id)
        {
            var result = await _trucksSubscribedRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }

        public async Task<FeedbackMessage> ChangeTruckSubscription(int subscriptionId, bool approved)
        {
            var subscriptionToUpdate = await _trucksSubscribedRepository.Get(subscriptionId);

            subscriptionToUpdate.Approved = approved;
            var result = await _trucksSubscribedRepository.Update(subscriptionToUpdate);
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }
    }
}
