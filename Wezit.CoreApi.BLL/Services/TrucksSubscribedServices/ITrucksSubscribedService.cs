﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TrucksSubscribedServices
{
    public interface ITrucksSubscribedService
    {
        Task<FeedbackMessage> GetTrucksSubscribed(int id);

        Task<FeedbackMessage> GetTrucksSubscribedList();
        Task<FeedbackMessage> GetTrucksSubscribedByCompany(int companyId);

        Task<FeedbackMessage> CreateTrucksSubscribed(TrucksSubscribedDTO trucksSubscribedDTO);

        Task<FeedbackMessage> UpdateTrucksSubscribed(int id, TrucksSubscribedDTO trucksSubscribedDTO);

        Task<FeedbackMessage> DeleteTrucksSubscribed(int id);
    }
}
