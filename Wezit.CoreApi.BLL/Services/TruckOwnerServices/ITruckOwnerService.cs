﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckOwnerServices
{
    public interface ITruckOwnerService
    {
        Task<FeedbackMessage> GetTruckOwner(long id);

        Task<FeedbackMessage> GetTruckOwnersList();

        Task<FeedbackMessage> CreateTruckOwner(TruckOwnerDTO truckOwnerDTO);

        Task<FeedbackMessage> UpdateTruckOwner(long id, TruckOwnerDTO truckOwnerDTO);

        Task<FeedbackMessage> DeleteTruckOwner(long id);

    }
}
