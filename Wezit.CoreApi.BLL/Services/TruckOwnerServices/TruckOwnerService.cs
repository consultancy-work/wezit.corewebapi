﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckOwnerServices
{
    public class TruckOwnerService : ITruckOwnerService
    {
        private readonly IGenericRepository<TruckOwner> _truckOwnerRepository;
        private readonly wezitContext _context;

        public TruckOwnerService(IGenericRepository<TruckOwner> truckOwnerRepository, wezitContext context)
        {
            _truckOwnerRepository = truckOwnerRepository;
            _context = context;

        }

        public async Task<FeedbackMessage> GetTruckOwner(long id)
        {
            var result = await _context.TruckOwners.Where(x => x.TruckCompanyId.Equals(id))
                .Select(x => new TruckOwnerDTO
                {
                    City = x.City,
                    TruckCompanyId = x.TruckCompanyId,
                    CompanyName = x.CompanyName,
                    ContactNumber = x.ContactNumber,
                    ContactPersonFirstName = x.ContactPersonFirstName,
                    ContactPersonLastName = x.ContactPersonLastName,
                    CountryId = x.CountryId,
                    EmailAddress = x.EmailAddress,
                    PhysicalAddress = x.PhysicalAddress,
                    PostalAddress = x.PostalAddress,
                    CountryName = x.Country.CountryName,
                    ServiceProviders = _context.TruckOwnerServiceProviders.Where(y => y.TruckOwnerId.Equals(x.TruckCompanyId))
                    .Select(z => new ServiceProviderDTO
                    {
                        ServiceProviderId = z.ServiceProviderId,
                        ServiceProviderName = z.ServiceProvider.ServiceProviderName,
                        ServiceProviderTypeCode = z.ServiceProvider.ServiceProviderTypeCode                        
                    })
                }).FirstOrDefaultAsync();

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetTruckOwnersList()
        {
            var data = await _truckOwnerRepository.GetAll();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateTruckOwner(TruckOwnerDTO truckOwnerDTO)
        {
            var truckOwner = new TruckOwner
            {
                CompanyName = truckOwnerDTO.CompanyName,
                ContactNumber = truckOwnerDTO.ContactNumber,
                ContactPersonFirstName = truckOwnerDTO.ContactPersonFirstName,
                ContactPersonLastName = truckOwnerDTO.ContactPersonLastName,
                EmailAddress = truckOwnerDTO.EmailAddress,
                City = truckOwnerDTO.City,
                CountryId = truckOwnerDTO.CountryId,
                PhysicalAddress = truckOwnerDTO.PhysicalAddress,
                PostalAddress = truckOwnerDTO.PostalAddress
            };

            var result = await _truckOwnerRepository.Insert(truckOwner);
            return result;
        }

        public async Task<FeedbackMessage> UpdateTruckOwner(long id, TruckOwnerDTO truckOwnerDTO)
        {
            var feedbackMessage = await GetTruckOwner(id);
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }
            var truckOwner = feedbackMessage.Data as TruckOwner;

            truckOwner.CompanyName = truckOwnerDTO.CompanyName;
            truckOwner.ContactNumber = truckOwnerDTO.ContactNumber;
            truckOwner.ContactPersonFirstName = truckOwnerDTO.ContactPersonFirstName;
            truckOwner.ContactPersonLastName = truckOwnerDTO.ContactPersonLastName;
            truckOwner.EmailAddress = truckOwnerDTO.EmailAddress;
            truckOwner.City = truckOwnerDTO.City;
            truckOwner.CountryId = truckOwnerDTO.CountryId;
            truckOwner.PhysicalAddress = truckOwnerDTO.PhysicalAddress;
            truckOwner.PostalAddress = truckOwnerDTO.PostalAddress;

            var result = await _truckOwnerRepository.Update(truckOwner);

            return result;
        }

        public async Task<FeedbackMessage> DeleteTruckOwner(long id)
        {
            var result = await _truckOwnerRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }

    }
}
