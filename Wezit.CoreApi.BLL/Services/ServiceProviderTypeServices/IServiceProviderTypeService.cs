﻿using System.Threading.Tasks;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.ServiceProviderTypeServices
{
    public interface IServiceProviderTypeService
    {
        Task<FeedbackMessage> GetServiceProviderType(string code);
        Task<FeedbackMessage> GetServiceProviderTypesList();
        Task<FeedbackMessage> GetServiceProviderTypeTypeList();
    }
}