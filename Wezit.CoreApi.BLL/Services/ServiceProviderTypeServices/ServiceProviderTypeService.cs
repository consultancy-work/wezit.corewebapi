﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.ServiceProviderTypeServices
{
    public class ServiceProviderTypeService : IServiceProviderTypeService
    {
        private readonly wezitContext _context;

        public ServiceProviderTypeService(wezitContext context)
        {
            _context = context;
        }

        public async Task<FeedbackMessage> GetServiceProviderType(string code)
        {
            var result = await _context.ServiceProviderTypes.FirstOrDefaultAsync(x => x.ProviderTypeCode.Equals(code));

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetServiceProviderTypeTypeList()
        {
            var data = await _context.ServiceProviderTypes.ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
        public async Task<FeedbackMessage> GetServiceProviderTypesList()
        {
            var data = await _context.ServiceProviderTypes.ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
    }
}
