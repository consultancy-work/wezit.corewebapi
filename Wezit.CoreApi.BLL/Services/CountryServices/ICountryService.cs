﻿using System.Threading.Tasks;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.CountryServices
{
    public interface ICountryService
    {
        Task<FeedbackMessage> GetCountriesList();
        Task<FeedbackMessage> GetCountry(int id);
    }
}