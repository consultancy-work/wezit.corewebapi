﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.CountryServices
{
    public class CountryService : ICountryService
    {
        private readonly IGenericRepository<Country> _countryRepository;

        public CountryService(IGenericRepository<Country> countryRepository)
        {
            _countryRepository = countryRepository;

        }

        public async Task<FeedbackMessage> GetCountry(int id)
        {
            var result = await _countryRepository.Get(id);

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = result
            };
        }

        public async Task<FeedbackMessage> GetCountriesList()
        {
            var data = await _countryRepository.GetAll();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
    }
}
