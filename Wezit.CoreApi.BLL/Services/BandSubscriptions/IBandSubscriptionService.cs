﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.BandSubscriptions
{
    public interface IBandSubscriptionService
    {
        Task<FeedbackMessage> GetBandSubscription(long id);

        Task<FeedbackMessage> GetBandSubscriptionsList();

        Task<FeedbackMessage> CreateBandSubscription(BandSubscriptionDTO bandSubscriptionDTO);

        Task<FeedbackMessage> UpdateBandSubscription(long id, BandSubscriptionDTO bandSubscriptionDTO);

        Task<FeedbackMessage> DeleteBandSubscription(long id);
    }
}
