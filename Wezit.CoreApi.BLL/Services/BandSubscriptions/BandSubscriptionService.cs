﻿using System;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.BandSubscriptions
{
    public class BandSubscriptionService : IBandSubscriptionService
    {
        private readonly IGenericRepository<BandSubscription> _bandSubscriptionRepository;

        public BandSubscriptionService(IGenericRepository<BandSubscription> bandSubscriptionRepository)
        {
            _bandSubscriptionRepository = bandSubscriptionRepository;

        }

        public async Task<FeedbackMessage> GetBandSubscription(long id)
        {
            var result = await _bandSubscriptionRepository.Get(Convert.ToInt32(id));

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetBandSubscriptionsList()
        {
            var data = await _bandSubscriptionRepository.GetAll();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateBandSubscription(BandSubscriptionDTO bandSubscriptionDTO)
        {
            var bandSubscription = new BandSubscription
            {
                BandName = bandSubscriptionDTO.BandName,
                BandRange = bandSubscriptionDTO.BandRange
            };

            var result = await _bandSubscriptionRepository.Insert(bandSubscription);
            return result;
        }

        public async Task<FeedbackMessage> UpdateBandSubscription(long id, BandSubscriptionDTO bandSubscriptionDTO)
        {
            var feedbackMessage = await GetBandSubscription(id);
            if(feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }

            var bandSubscription = feedbackMessage.Data as BandSubscription;
            bandSubscription.BandName = bandSubscriptionDTO.BandName;
            bandSubscription.BandRange = bandSubscriptionDTO.BandRange;

            var result = await _bandSubscriptionRepository.Update(bandSubscription);

            return result;
        }

        public async Task<FeedbackMessage> DeleteBandSubscription(long id)
        {
            var result = await _bandSubscriptionRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }
    }
}
