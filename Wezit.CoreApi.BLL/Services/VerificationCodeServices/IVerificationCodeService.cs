﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.VerificationCodeServices
{
    public interface IVerificationCodeService
    {
        Task<FeedbackMessage> CreateVerificationCode(VerificationCodeDTO verificationCodeDTO);
        Task<FeedbackMessage> GetVerificationCode(long id);
        Task<FeedbackMessage> VerifyCode(string phoneNumber, int code);
        Task<FeedbackMessage> VerifyEmail(int customerId);
    }
}