﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.VerificationCodeServices
{
    public class VerificationCodeService : IVerificationCodeService
    {

        private readonly IGenericRepository<VerificationCode> _verificationCodeRepository;
        private readonly wezitContext _context;

        public VerificationCodeService(IGenericRepository<VerificationCode> verificationCodeRepository,
            wezitContext context)
        {
            _verificationCodeRepository = verificationCodeRepository;
            _context = context;
        }

        public async Task<FeedbackMessage> GetVerificationCode(long id)
        {
            var result = await _verificationCodeRepository.Get(Convert.ToInt32(id));

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> VerifyCode(string phoneNumber, int code)
        {

            var phone_number_code = await _context.VerificationCodes.Where(x => x.PhoneNumber.Equals(phoneNumber)).OrderByDescending(x => x.CodeId).FirstOrDefaultAsync();

            if (phone_number_code is null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "Invalid Code."
                };
            }
           
            return new FeedbackMessage
            {
                HasErrorOccured = phone_number_code.Code.Equals(code.ToString()) ? false : true,
                Message = phone_number_code.Code.Equals(code.ToString()) ? "User Verified" : "Invalid code"
            };
        }
        public async Task<FeedbackMessage> VerifyEmail(int customerId)
        {
            var customer = await _context.Customers.FirstOrDefaultAsync(x => x.CustomerId.Equals(customerId));
            if(customer is null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "customer doesnt exist."
                };
            }
            customer.IsVerified = true;
            _context.Customers.Update(customer);
            await _context.SaveChangesAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false
            };
        }
        public async Task<FeedbackMessage> CreateVerificationCode(VerificationCodeDTO verificationCodeDTO)
        {
            var verificationCode = new VerificationCode
            {
                CodeId = verificationCodeDTO.CodeId,
                PhoneNumber = verificationCodeDTO.PhoneNumber,
                Code = verificationCodeDTO.Code
            };

            var result = await _verificationCodeRepository.Insert(verificationCode);
            return result;
        }

    }
}
