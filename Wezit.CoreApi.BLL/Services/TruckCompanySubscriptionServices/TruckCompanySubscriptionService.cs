﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckCompanySubscriptionServices
{
    public class TruckCompanySubscriptionService : ITruckCompanySubscriptionService
    {
        private readonly IGenericRepository<TruckCompanySubscription> _truckCompanySubscriptionRepository;

        public TruckCompanySubscriptionService(IGenericRepository<TruckCompanySubscription> truckCompanySubscriptionRepository)
        {
            _truckCompanySubscriptionRepository = truckCompanySubscriptionRepository;

        }

        public async Task<FeedbackMessage> GetTruckCompanySubscription(long id)
        {
            var relationsToQuery = new string[] { "Band", "TruckCompany" };
            var result = await _truckCompanySubscriptionRepository.Get("TruckSubscriptionId", Convert.ToString(id), 1, SearchType.Numeric, relationsToQuery);

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }
        public async Task<FeedbackMessage> GetTruckCompanySubscriptionByCompany(long companyId)
        {
            var relationsToQuery = new string[] { "Band", "TruckCompany" };
            var result = await _truckCompanySubscriptionRepository.GetList("TruckCompanyId", Convert.ToString(companyId), 1, SearchType.Numeric, relationsToQuery);

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetTruckCompanySubscriptionsList()
        {
            var relationsToQuery = new string[] { "Band", "TruckCompany" };
            var data = await _truckCompanySubscriptionRepository.GetList("","", 100, SearchType.Numeric, relationsToQuery);

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateTruckCompanySubscription(TruckCompanySubscriptionDTO truckCompanySubscriptionDTO)
        {
            var truckCompanySubscription = new TruckCompanySubscription
            {
                TruckCompanyId = truckCompanySubscriptionDTO.TruckCompanyId,
                BandId = truckCompanySubscriptionDTO.BandId,
                SubscriptionStartDate = truckCompanySubscriptionDTO.SubscriptionStartDate,
                SubscriptionEndDate = truckCompanySubscriptionDTO.SubscriptionEndDate
            };

            var result = await _truckCompanySubscriptionRepository.Insert(truckCompanySubscription);
            return result;
        }

        public async Task<FeedbackMessage> UpdateTruckCompanySubscription(long id, TruckCompanySubscriptionDTO truckCompanySubscriptionDTO)
        {
            var feedbackMessage = await GetTruckCompanySubscription(id);
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }
            var truckCompanySubscription = feedbackMessage.Data as TruckCompanySubscription;

            truckCompanySubscription.TruckCompanyId = truckCompanySubscriptionDTO.TruckCompanyId;
            truckCompanySubscription.BandId = truckCompanySubscriptionDTO.BandId;
            truckCompanySubscription.SubscriptionStartDate = truckCompanySubscriptionDTO.SubscriptionStartDate;
            truckCompanySubscription.SubscriptionEndDate = truckCompanySubscriptionDTO.SubscriptionEndDate;

            var result = await _truckCompanySubscriptionRepository.Update(truckCompanySubscription);

            return result;
        }

        public async Task<FeedbackMessage> DeleteTruckCompanySubscription(long id)
        {
            var result = await _truckCompanySubscriptionRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }

    }
}
