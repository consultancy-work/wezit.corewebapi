﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.TruckCompanySubscriptionServices
{
    public interface ITruckCompanySubscriptionService
    {
        Task<FeedbackMessage> GetTruckCompanySubscription(long id);
        Task<FeedbackMessage> GetTruckCompanySubscriptionByCompany(long id);

        Task<FeedbackMessage> GetTruckCompanySubscriptionsList();

        Task<FeedbackMessage> CreateTruckCompanySubscription(TruckCompanySubscriptionDTO truckCompanySubscriptionDTO);

        Task<FeedbackMessage> UpdateTruckCompanySubscription(long id, TruckCompanySubscriptionDTO truckCompanySubscriptionDTO);

        Task<FeedbackMessage> DeleteTruckCompanySubscription(long id);
    }
}
