﻿using System;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.EventLogStageServices
{
    public class EventLogStageService: IEventLogStageService
    {
        private readonly IGenericRepository<EventLogStage> _eventLogStageRepository;

        public EventLogStageService(IGenericRepository<EventLogStage> eventLogStageRepository)
        {
            _eventLogStageRepository = eventLogStageRepository;

        }

        public async Task<FeedbackMessage> GetEventLogStage(long id)
        {
            var result = await _eventLogStageRepository.Get(Convert.ToInt32(id));

            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetEventLogStagesList()
        {
            var data = await _eventLogStageRepository.GetAll();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateEventLogStage(EventLogStageDTO eventLogStageDTO)
        {
            var eventLogStage = new EventLogStage
            {
                Description = eventLogStageDTO.Description
            };

            var result = await _eventLogStageRepository.Insert(eventLogStage);
            return result;
        }

        public async Task<FeedbackMessage> UpdateEventLogStage(long id, EventLogStageDTO eventLogStageDTO)
        {
            var feedbackMessage = await GetEventLogStage(id);
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }
            var eventLogStage = feedbackMessage.Data as EventLogStage;

            eventLogStage.Description = eventLogStageDTO.Description;

            var result = await _eventLogStageRepository.Update(eventLogStage);

            return result;
        }

        public async Task<FeedbackMessage> DeleteEventLogStage(long id)
        {
            var result = await _eventLogStageRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }
    }
}
