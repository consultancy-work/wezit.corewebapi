﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.EventLogStageServices
{
    public interface IEventLogStageService
    {
        Task<FeedbackMessage> GetEventLogStage(long id);

        Task<FeedbackMessage> GetEventLogStagesList();

        Task<FeedbackMessage> CreateEventLogStage(EventLogStageDTO eventLogStageDTO);

        Task<FeedbackMessage> UpdateEventLogStage(long id, EventLogStageDTO eventLogStageDTO);

        Task<FeedbackMessage> DeleteEventLogStage(long id);
    }
}
