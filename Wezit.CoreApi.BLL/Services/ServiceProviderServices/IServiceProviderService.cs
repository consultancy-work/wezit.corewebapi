﻿using System.Threading.Tasks;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;

namespace Wezit.CoreApi.BLL.Services.ServiceProvidersServices
{
    public interface IServiceProviderService
    {
        Task<FeedbackMessage> CreateServiceProvider(ServiceProviderDTO serviceProviderDTO);
        Task<FeedbackMessage> DeleteServiceProvider(long id);
        Task<FeedbackMessage> GetServiceProvider(long id);
        Task<FeedbackMessage> GetServiceProviderByTypeList(string code);
        Task<FeedbackMessage> GetServiceProvidersList();
        Task<FeedbackMessage> UpdateServiceProvider(long id, ServiceProviderDTO serviceProviderDTO);
    }
}