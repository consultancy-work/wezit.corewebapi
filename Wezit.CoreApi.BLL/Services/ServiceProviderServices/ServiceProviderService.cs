﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wezit.DAL;
using Wezit.DAL.Models;
using Wezit.DataModels.DTO;
using Wezit.DataModels.General;
using Wezit.DataModels.ViewModels;

namespace Wezit.CoreApi.BLL.Services.ServiceProvidersServices
{
    public class ServiceProviderService : IServiceProviderService
    {
        private readonly IGenericRepository<ServiceProvider> _serviceProviderRepository;
        private readonly wezitContext _context;

        public ServiceProviderService(IGenericRepository<ServiceProvider> serviceProviderRepository, wezitContext context)
        {
            _serviceProviderRepository = serviceProviderRepository;
            _context = context;
        }

        public async Task<FeedbackMessage> GetServiceProvider(long id)
        {
            var result = await _context.ServiceProviders.Where(x => x.ServiceProviderId.Equals(id))
                .Select(x => new ServiceProviderViewModel 
                {
                    ServiceProviderId = x.ServiceProviderId,
                    ServiceProviderName = x.ServiceProviderName,
                    ServiceProviderTypes = _context.ServiceProviderTypes.Select(y => new ServiceProviderTypeDTO 
                    { 
                        ProviderTypeCode = y.ProviderTypeCode, 
                        ProviderTypeName = y.ProviderTypeName
                    }).ToList()
                }).FirstOrDefaultAsync();
            return new FeedbackMessage { HasErrorOccured = false, Data = result };
        }

        public async Task<FeedbackMessage> GetServiceProviderByTypeList(string code)
        {
            var data = await _context.ServiceProviders.Where(x => x.ServiceProviderTypeCode.Equals(code)).ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }
        public async Task<FeedbackMessage> GetServiceProvidersList()
        {
            var data = await _context.ServiceProviders.ToListAsync();

            return new FeedbackMessage
            {
                HasErrorOccured = false,
                Data = data
            };
        }

        public async Task<FeedbackMessage> CreateServiceProvider(ServiceProviderDTO serviceProviderDTO)
        {
            var serviceProvider = new ServiceProvider
            {
                ServiceProviderName = serviceProviderDTO.ServiceProviderName,
                ServiceProviderTypeCode = serviceProviderDTO.ServiceProviderTypeCode
            };

            var result = await _serviceProviderRepository.Insert(serviceProvider);
            return result;
        }

        public async Task<FeedbackMessage> UpdateServiceProvider(long id, ServiceProviderDTO serviceProviderDTO)
        {
            var feedbackMessage = await GetServiceProvider(id);
            if (feedbackMessage.Data == null)
            {
                return new FeedbackMessage
                {
                    HasErrorOccured = true,
                    Message = "The item with that ID was not found"
                };
            }
            var serviceProvider = feedbackMessage.Data as ServiceProvider;

            serviceProvider.ServiceProviderName = serviceProviderDTO.ServiceProviderName;
            serviceProvider.ServiceProviderTypeCode = serviceProviderDTO.ServiceProviderTypeCode;

            var result = await _serviceProviderRepository.Update(serviceProvider);

            return result;
        }

        public async Task<FeedbackMessage> DeleteServiceProvider(long id)
        {
            var result = await _serviceProviderRepository.Delete(Convert.ToInt32(id));
            if (result.HasErrorOccured)
            {
                return result;
            }

            return result;
        }

    }
}
