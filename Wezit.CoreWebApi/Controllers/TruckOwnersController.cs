﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.TruckOwnerServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class TruckOwnersController : ControllerBase
    {
        private readonly ITruckOwnerService _truckOwnerService;

        public TruckOwnersController(ITruckOwnerService truckOwnerService)
        {
            _truckOwnerService = truckOwnerService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _truckOwnerService.GetTruckOwner(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _truckOwnerService.GetTruckOwnersList();

            return Ok(feedbackMessage.Data);
        }

       
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] TruckOwnerDTO truckOwnerDTO)
        {
            var feedbackMessage = await _truckOwnerService.CreateTruckOwner(truckOwnerDTO);
            if(feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(long id, [FromBody] TruckOwnerDTO truckOwnerDTO)
        {
            var feedbackMessage = await _truckOwnerService.UpdateTruckOwner(id, truckOwnerDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _truckOwnerService.DeleteTruckOwner(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }


    }
}
