﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.ServiceProviderTypeServices;
using Wezit.CoreWebApi.CustomFilters;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class ServiceProviderTypesController : ControllerBase
    {
        private readonly IServiceProviderTypeService _serviceProviderTypeService;

        public ServiceProviderTypesController(IServiceProviderTypeService serviceProviderTypeService)
        {
            _serviceProviderTypeService = serviceProviderTypeService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(string code)
        {
            var feedbackMessage = await _serviceProviderTypeService.GetServiceProviderType(code);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _serviceProviderTypeService.GetServiceProviderTypesList();

            return Ok(feedbackMessage.Data);
        }
    }
}
