﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.ServiceProvidersServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class ServiceProvidersController : ControllerBase
    {
        private readonly IServiceProviderService _serviceProviderService;

        public ServiceProvidersController(IServiceProviderService serviceProviderService)
        {
            _serviceProviderService = serviceProviderService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _serviceProviderService.GetServiceProvider(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _serviceProviderService.GetServiceProvidersList();

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetServiceProviderByTypeList")]
        public async Task<IActionResult> GetServiceProviderTypeList(string code)
        {
            var feedbackMessage = await _serviceProviderService.GetServiceProviderByTypeList(code);

            return Ok(feedbackMessage.Data);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] ServiceProviderDTO serviceProviderDTO)
        {
            var feedbackMessage = await _serviceProviderService.CreateServiceProvider(serviceProviderDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(long id, [FromBody] ServiceProviderDTO serviceProviderDTO)
        {
            var feedbackMessage = await _serviceProviderService.UpdateServiceProvider(id, serviceProviderDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _serviceProviderService.DeleteServiceProvider(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }
    }
}
