﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.CustomerServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _customerService.GetCustomer(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _customerService.GetCustomersList();

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] CustomerDTO customerDTO)
        {
            var feedbackMessage = await _customerService.CreateCustomer(customerDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("",feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromQuery] long id, [FromBody] CustomerDTO customerDTO)
        {
            var feedbackMessage = await _customerService.UpdateCustomer(id, customerDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _customerService.DeleteCustomer(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

    }
}
