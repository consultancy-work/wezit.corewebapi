﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.TruckCompanySubscriptionServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class TruckCompanySubscriptionsController : ControllerBase
    {
        private readonly ITruckCompanySubscriptionService _truckCompanySubscriptionService;

        public TruckCompanySubscriptionsController(ITruckCompanySubscriptionService truckCompanySubscriptionService)
        {
            _truckCompanySubscriptionService = truckCompanySubscriptionService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _truckCompanySubscriptionService.GetTruckCompanySubscription(id);

            return Ok(feedbackMessage.Data);
        }
        [HttpGet("GetByCompany")]
        public async Task<IActionResult> GetByCompany(long companyId)
        {
            var feedbackMessage = await _truckCompanySubscriptionService.GetTruckCompanySubscriptionByCompany(companyId);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _truckCompanySubscriptionService.GetTruckCompanySubscriptionsList();

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] TruckCompanySubscriptionDTO truckCompanySubscriptionDTO)
        {
            var feedbackMessage = await _truckCompanySubscriptionService.CreateTruckCompanySubscription(truckCompanySubscriptionDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(long id, [FromBody] TruckCompanySubscriptionDTO truckCompanySubscriptionDTO)
        {
            var feedbackMessage = await _truckCompanySubscriptionService.UpdateTruckCompanySubscription(id, truckCompanySubscriptionDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _truckCompanySubscriptionService.DeleteTruckCompanySubscription(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

    }
}
