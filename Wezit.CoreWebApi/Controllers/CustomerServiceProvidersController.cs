﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.ServiceProvidersServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class CustomerServiceProvidersController : ControllerBase
    {
        private readonly ICustomerServiceProviderService _customerServiceProviderService;

        public CustomerServiceProvidersController(ICustomerServiceProviderService customerServiceProviderService)
        {
            _customerServiceProviderService = customerServiceProviderService;
        }

        [HttpGet("GetCustomerServiceProvider")]
        public async Task<IActionResult> GetCustomerServiceProvider(int companyId)
        {
            var feedbackMessage = await _customerServiceProviderService.GetCustomerServiceProvider(companyId);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetCustomerServiceProviderByBooking")]
        public async Task<IActionResult> GetCustomerServiceProviderByBooking(int bookingId)
        {
            var feedbackMessage = await _customerServiceProviderService.GetCustomerServiceProviderByBooking(bookingId);

            return Ok(feedbackMessage.Data);
        }
        
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] IEnumerable<CustomerServiceProviderDTO> customerServiceProviders)
        {
            var feedbackMessage = await _customerServiceProviderService.CreateCustomerServiceProvider(customerServiceProviders);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromBody] IEnumerable<CustomerServiceProviderDTO> customerServiceProviders)
        {
            var feedbackMessage = await _customerServiceProviderService.UpdateCustomerServiceProvider(customerServiceProviders);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }


    }
}
