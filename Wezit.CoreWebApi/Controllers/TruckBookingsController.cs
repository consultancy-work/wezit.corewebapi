﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.TruckBookingServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class TruckBookingsController : ControllerBase
    {
        private readonly ITruckBookingService _truckBookingService;

        public TruckBookingsController(ITruckBookingService truckBookingService)
        {
            _truckBookingService = truckBookingService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(int id)
        {
            var feedbackMessage = await _truckBookingService.GetTruckBooking(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetByCustomer")]
        public async Task<IActionResult> GetByCustomer(int id)
        {
            var feedbackMessage = await _truckBookingService.GetByCustomer(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetActiveByCompany")]
        public async Task<IActionResult> GetActiveByCompany(int id)
        {
            var feedbackMessage = await _truckBookingService.GetActiveByCompany(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetByCompany")]
        public async Task<IActionResult> GetByCompany(int id)
        {
            var feedbackMessage = await _truckBookingService.GetByCompany(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetActiveByCustomer")]
        public async Task<IActionResult> GetActiveByCustomer(int id)
        {
            var feedbackMessage = await _truckBookingService.GetActiveByCustomer(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _truckBookingService.GetTruckBookingsList();

            return Ok(feedbackMessage.Data);
        }
        

        [HttpGet("GetActiveList")]
        public async Task<IActionResult> GetActiveList()
        {
            var feedbackMessage = await _truckBookingService.GetActiveList();

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] TruckBookingRequest truckBookingRequest)
        {
            var feedbackMessage = await _truckBookingService.CreateTruckBooking(truckBookingRequest);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(int id, int eventStageId, [FromBody] TruckBookingDTO truckBookingDTO)
        {
            var feedbackMessage = await _truckBookingService.UpdateTruckBooking(id,eventStageId, truckBookingDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var feedbackMessage = await _truckBookingService.DeleteTruckBooking(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }
    }
}
