﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.VerificationCodeServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class VerificationCodesController : ControllerBase
    {
        private readonly IVerificationCodeService _verificationCodeService;

        public VerificationCodesController(IVerificationCodeService verificationCodeService)
        {
            _verificationCodeService = verificationCodeService;
        }
        
        [HttpGet("Verify")]
        public async Task<IActionResult> Verify(string phoneNumber, int code)
        {
            var feedbackMessage = await _verificationCodeService.VerifyCode(phoneNumber, code);

            return Ok(feedbackMessage);
        }
        
        [HttpGet("VerifyEmail")]
        public async Task<IActionResult> VerifyEmail(int customerId)
        {
            var feedbackMessage = await _verificationCodeService.VerifyEmail(customerId);

            return Ok(feedbackMessage.Data);
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] VerificationCodeDTO verificationCodeDTO)
        {
            var feedbackMessage = await _verificationCodeService.CreateVerificationCode(verificationCodeDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }
    }
}
