﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.ServiceProvidersServices;
using Wezit.CoreApi.BLL.Services.TruckOwnerServiceProvidersServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class TruckOwnerServiceProvidersController : ControllerBase
    {
        private readonly ITruckOwnerServiceProviderService _truckOwnerServiceProviderService;

        public TruckOwnerServiceProvidersController(ITruckOwnerServiceProviderService truckOwnerServiceProviderService)
        {
            _truckOwnerServiceProviderService = truckOwnerServiceProviderService;
        }

        [HttpGet("GetCustomerServiceProvider")]
        public async Task<IActionResult> GetCustomerServiceProvider(int truckCompanyId)
        {
            var feedbackMessage = await _truckOwnerServiceProviderService.GetTruckOwnerServiceProvider(truckCompanyId);

            return Ok(feedbackMessage.Data);
        }
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] IEnumerable<TruckOwnerServiceProviderDTO> truckOwnerServiceProviders)
        {
            var feedbackMessage = await _truckOwnerServiceProviderService.CreateTruckOwnerServiceProvider(truckOwnerServiceProviders);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromBody] IEnumerable<TruckOwnerServiceProviderDTO> truckOwnerServiceProviders)
        {
            var feedbackMessage = await _truckOwnerServiceProviderService.UpdateTruckOwnerServiceProvider(truckOwnerServiceProviders);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

    }
}
