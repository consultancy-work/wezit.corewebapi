﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.TrucksServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class TrucksController : ControllerBase
    {
        private readonly ITruckService _truckService;

        public TrucksController(ITruckService truckService)
        {
            _truckService = truckService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _truckService.GetTruck(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _truckService.GetTrucksList();

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetCategoryList")]
        public async Task<IActionResult> GetCategoryList()
        {
            var feedbackMessage = await _truckService.GetCategoryList();

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetTruckTypeList")]
        public async Task<IActionResult> GetTruckTypeList()
        {
            var feedbackMessage = await _truckService.GetTruckTypeList();

            return Ok(feedbackMessage.Data);
        }
        [HttpGet("GetTrucksByCompany")]
        public async Task<IActionResult> GetTrucksByCompany(int companyId)
        {
            var feedbackMessage = await _truckService.GetTrucksByCompany(companyId);

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] TruckDTO truckDTO)
        {
            var feedbackMessage = await _truckService.CreateTruck(truckDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(long id, [FromBody] TruckDTO truckDTO)
        {
            var feedbackMessage = await _truckService.UpdateTruck(id, truckDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _truckService.DeleteTruck(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }
    }
}
