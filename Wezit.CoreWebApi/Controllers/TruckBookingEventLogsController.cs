﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.TruckBookingEventLogServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class TruckBookingEventLogsController : ControllerBase
    {
        private readonly ITruckBookingEventLogService _truckBookingEventLogService;

        public TruckBookingEventLogsController(ITruckBookingEventLogService truckBookingEventLogService)
        {
            _truckBookingEventLogService = truckBookingEventLogService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _truckBookingEventLogService.GetTruckBookingEventLog(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _truckBookingEventLogService.GetTruckBookingEventLogsList();

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] TruckBookingEventLogDTO truckBookingEventLogDTO)
        {
            var feedbackMessage = await _truckBookingEventLogService.CreateTruckBookingEventLog(truckBookingEventLogDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(long id, [FromBody] TruckBookingEventLogDTO truckBookingEventLogDTO)
        {
            var feedbackMessage = await _truckBookingEventLogService.UpdateTruckBookingEventLog(id, truckBookingEventLogDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _truckBookingEventLogService.DeleteTruckBookingEventLog(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }
    }
}
