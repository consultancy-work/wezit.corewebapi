﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.BandSubscriptions;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class BandSubscriptionsController : ControllerBase
    {
        private readonly IBandSubscriptionService _bandSubscriptionService;

        public BandSubscriptionsController(IBandSubscriptionService bandSubscriptionService)
        {
            _bandSubscriptionService = bandSubscriptionService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _bandSubscriptionService.GetBandSubscription(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _bandSubscriptionService.GetBandSubscriptionsList();

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] BandSubscriptionDTO bandSubscriptionDTO)
        {
            var feedbackMessage = await _bandSubscriptionService.CreateBandSubscription(bandSubscriptionDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(long id, [FromBody] BandSubscriptionDTO bandSubscriptionDTO)
        {
            var feedbackMessage = await _bandSubscriptionService.UpdateBandSubscription(id, bandSubscriptionDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _bandSubscriptionService.DeleteBandSubscription(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }
    }
}
