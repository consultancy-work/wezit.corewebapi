﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.EventLogStageServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class EventLogStagesController : ControllerBase
    {
        private readonly IEventLogStageService _eventLogStageService;

        public EventLogStagesController(IEventLogStageService eventLogStageService)
        {
            _eventLogStageService = eventLogStageService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(long id)
        {
            var feedbackMessage = await _eventLogStageService.GetEventLogStage(id);

            return Ok(feedbackMessage.Data);
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _eventLogStageService.GetEventLogStagesList();

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] EventLogStageDTO eventLogStageDTO)
        {
            var feedbackMessage = await _eventLogStageService.CreateEventLogStage(eventLogStageDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(long id, [FromBody] EventLogStageDTO eventLogStageDTO)
        {
            var feedbackMessage = await _eventLogStageService.UpdateEventLogStage(id, eventLogStageDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(long id)
        {
            var feedbackMessage = await _eventLogStageService.DeleteEventLogStage(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }
    }
}
