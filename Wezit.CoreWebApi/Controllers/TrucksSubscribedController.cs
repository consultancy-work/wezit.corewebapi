﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wezit.CoreApi.BLL.Services.TrucksSubscribedServices;
using Wezit.CoreWebApi.CustomFilters;
using Wezit.DataModels.DTO;

namespace Wezit.CoreWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateModel]
    public class TrucksSubscribedController : ControllerBase
    {
        private readonly ITrucksSubscribedService _trucksSubscribedService;

        public TrucksSubscribedController(ITrucksSubscribedService trucksSubscribedService)
        {
            _trucksSubscribedService = trucksSubscribedService;
        }

        [HttpGet("GetSingle")]
        public async Task<IActionResult> GetSingle(int id)
        {
            var feedbackMessage = await _trucksSubscribedService.GetTrucksSubscribed(id);

            return Ok(feedbackMessage.Data);
        }
        [HttpGet("GetByCompany")]
        public async Task<IActionResult> GetByCompany(int companyId)
        {
            var feedbackMessage = await _trucksSubscribedService.GetTrucksSubscribedByCompany(companyId);

            return Ok(feedbackMessage.Data);
        }
        [HttpGet("AvailableTrucksSubscribed")]
        public async Task<IActionResult> GetAvailableTrucksSubscribed(int companyId)
        {
           // var feedbackMessage = await _trucksSubscribedService.GetAvailableTrucksSubscribed(companyId);

            return Ok();
        }

        [HttpGet("GetList")]
        public async Task<IActionResult> GetList()
        {
            var feedbackMessage = await _trucksSubscribedService.GetTrucksSubscribedList();

            return Ok(feedbackMessage.Data);
        }


        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] TrucksSubscribedDTO trucksSubscribedDTO)
        {
            var feedbackMessage = await _trucksSubscribedService.CreateTrucksSubscribed(trucksSubscribedDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Created("", feedbackMessage);
        }

        [HttpPut("Update")]
        public async Task<IActionResult> Update(int id, [FromBody] TrucksSubscribedDTO trucksSubscribedDTO)
        {
            var feedbackMessage = await _trucksSubscribedService.UpdateTrucksSubscribed(id, trucksSubscribedDTO);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

        [HttpPost("Delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var feedbackMessage = await _trucksSubscribedService.DeleteTrucksSubscribed(id);
            if (feedbackMessage.HasErrorOccured)
            {
                return BadRequest(feedbackMessage);
            }

            return Ok(feedbackMessage);
        }

    }
}
