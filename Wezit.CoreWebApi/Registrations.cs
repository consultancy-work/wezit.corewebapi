﻿using Microsoft.Extensions.DependencyInjection;
using Wezit.CoreApi.BLL.Services.BandSubscriptions;
using Wezit.CoreApi.BLL.Services.CountryServices;
using Wezit.CoreApi.BLL.Services.CustomerServices;
using Wezit.CoreApi.BLL.Services.EventLogStageServices;
using Wezit.CoreApi.BLL.Services.ServiceProvidersServices;
using Wezit.CoreApi.BLL.Services.ServiceProviderTypeServices;
using Wezit.CoreApi.BLL.Services.TruckBookingEventLogServices;
using Wezit.CoreApi.BLL.Services.TruckBookingServices;
using Wezit.CoreApi.BLL.Services.TruckCompanySubscriptionServices;
using Wezit.CoreApi.BLL.Services.TruckOwnerServiceProvidersServices;
using Wezit.CoreApi.BLL.Services.TruckOwnerServices;
using Wezit.CoreApi.BLL.Services.TrucksServices;
using Wezit.CoreApi.BLL.Services.TrucksSubscribedServices;
using Wezit.CoreApi.BLL.Services.VerificationCodeServices;
using Wezit.DAL;
using Wezit.DAL.Models;
using ServiceProvider = Wezit.DAL.Models.ServiceProvider;

namespace Wezit.CoreWebApi
{
    public static class Registrations
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services)
        {
            services.AddScoped<ICustomerServiceProviderService, CustomerServiceProviderService>();
            services.AddScoped<ITruckOwnerServiceProviderService, TruckOwnerServiceProviderService>();
            services.AddScoped<IServiceProviderService, ServiceProviderService>();
            services.AddScoped<IServiceProviderTypeService, ServiceProviderTypeService>();
            services.AddScoped<IVerificationCodeService, VerificationCodeService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<ITruckOwnerService, TruckOwnerService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ITruckCompanySubscriptionService, TruckCompanySubscriptionService>();
            services.AddScoped<ITruckService, TruckService>();
            services.AddScoped<ITruckBookingService, TruckBookingService>();
            services.AddScoped<IBandSubscriptionService, BandSubscriptionService>();
            services.AddScoped<IEventLogStageService, EventLogStageService>();
            services.AddScoped<ITrucksSubscribedService, TrucksSubscribedService>();
            services.AddScoped<ITruckBookingEventLogService, TruckBookingEventLogService>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IGenericRepository<ServiceProvider>, GenericRepository<ServiceProvider>>();
            services.AddTransient<IGenericRepository<VerificationCode>, GenericRepository<VerificationCode>>();
            services.AddTransient<IGenericRepository<Country>, GenericRepository<Country>>();
            services.AddTransient<IGenericRepository<TruckOwner>, GenericRepository<TruckOwner>>();
            services.AddTransient<IGenericRepository<Customer>, GenericRepository<Customer>>();
            services.AddTransient<IGenericRepository<TruckCompanySubscription>, GenericRepository<TruckCompanySubscription>>();
            services.AddTransient<IGenericRepository<Truck>, GenericRepository<Truck>>();
            services.AddTransient<IGenericRepository<BandSubscription>, GenericRepository<BandSubscription>>();
            services.AddTransient<IGenericRepository<TruckBooking>, GenericRepository<TruckBooking>>();
            services.AddTransient<IGenericRepository<EventLogStage>, GenericRepository<EventLogStage>>();
            services.AddTransient<IGenericRepository<TrucksSubscribed>, GenericRepository<TrucksSubscribed>>();
            services.AddTransient<IGenericRepository<TruckBookingEventLog>, GenericRepository<TruckBookingEventLog>>();
            return services;
        }
    }
}
