﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using Wezit.DataModels.General;

namespace Wezit.CoreWebApi.CustomFilters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                //Get error messages and turn them to strings
                var errorMessages = context.ModelState.Keys
                                .SelectMany(key => context.ModelState[key].Errors.Select(x => new string(x.ErrorMessage)))
                                .ToList();

                //Return Badresult with a FeedbackMessage as the response content
                context.Result = new BadRequestObjectResult(new FeedbackMessage
                {
                    FeedbackType = FeedbackMessageType.VALIDATIONERROR,
                    Message = errorMessages.FirstOrDefault(),
                    Messages = errorMessages
                });
            }
        }
    }
}
